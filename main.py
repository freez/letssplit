import logging

from telegram import Update, InlineQueryResultArticle, InputTextMessageContent, InlineKeyboardButton, \
    InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, InlineQueryHandler, MessageHandler, Filters, \
    CallbackContext, \
    ChosenInlineResultHandler, CallbackQueryHandler

from Language import Language
from text import get_string

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update: Update, context: CallbackContext) -> None:
    if update.effective_chat.type == 'private':
        # todo check first use of this command or not
        keyboard = [[InlineKeyboardButton("فارسی", callback_data='setting#language#persian')],
                    [InlineKeyboardButton("English", callback_data='setting#language#english')]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text(
            'Setting Configuration:\nWhat is your language?',
            reply_markup=reply_markup)


def help_command(update: Update, context: CallbackContext) -> None:
    # todo check user language or group language
    update.message.reply_text(get_string("help", "fa"))


def on_result_chosen(bot, update):
    print(update.to_dict())
    result = update.chosen_inline_result
    result_id = result.result_id
    query = result.query
    user = result.from_user.id
    print(result_id)
    print(user)
    print(query)
    print(result.inline_message_id)
    bot.send_message(user, text='fetching book data with id:' + result_id)


def button(update: Update, context: CallbackContext) -> None:
    query = update.callback_query

    # CallbackQueries need to be answered, even if no notification to the user is needed
    # Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
    query.answer()

    print(f"Selected option: {query.data}")
    arguments = query.data.split('#')
    if arguments[0] == "setting":
        if arguments[1] == "language":
            # todo set chat language
            # todo show next step of setting
            if query.message.chat.type != query.message.chat.PRIVATE:
                lang = Language.English
                if arguments[2] == "persian":
                    lang = Language.Persian
                db.change_group_language(update.message.chat_id, lang)
                keyboard = [[InlineKeyboardButton("تومان", callback_data='setting#money#toman')],
                            [InlineKeyboardButton("ریال", callback_data='setting#money#rial')],
                            [InlineKeyboardButton("دلار", callback_data='setting#money#dolar')]]
                reply_markup = InlineKeyboardMarkup(keyboard)
                context.bot.edit_message_text(
                    chat_id=query.message.chat_id,
                    message_id=query.message.message_id,
                    text=get_string("choose_money_unit", "fa"),
                    reply_markup=reply_markup)
        elif arguments[1] == "money":
            keyboard = [[InlineKeyboardButton("بله", callback_data='setting#simplify#yes'),
                         InlineKeyboardButton("خیر", callback_data='setting#simplify#no')]]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.edit_message_text(
                chat_id=query.message.chat_id,
                message_id=query.message.message_id,
                text=get_string("setting_simplify", "fa"),
                reply_markup=reply_markup)

    # query.edit_message_text(text=f"Selected option: {query.data}")


def get_inline_results(update, context):
    query = update.inline_query.query
    print(update)
    results = list()
    splited_query = str(query).split(' ')
    amount = splited_query[0]
    description = ' '.join(splited_query[1:])
    results.append(InlineQueryResultArticle(id='1000',
                                            title="I Paid",
                                            description='Amount: ' + amount + '\n' + 'Description: ' + description,
                                            # thumb_url='https://fakeimg.pl/100/?text=book%201',
                                            input_message_content=InputTextMessageContent(
                                                'chosen book:')))

    results.append(InlineQueryResultArticle(id='1001',
                                            title="Others paid",
                                            description='Amount: ' + amount + '\n' + 'Description: ' + description,
                                            # thumb_url='https://fakeimg.pl/300/?text=book%202',
                                            input_message_content=InputTextMessageContent(
                                                'chosen book:')
                                            ))

    update.inline_query.answer(results)


def new_member(update: Update, context: CallbackContext):
    for member in update.message.new_chat_members:
        if member.username == 'LetsSplitBot':
            db.add_new_group(update.message.chat_id, update.message.chat.title)
            keyboard = [[InlineKeyboardButton("فارسی", callback_data='setting#language#persian')],
                        [InlineKeyboardButton("English", callback_data='setting#language#english')]]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.message.chat_id,
                                     text='Hi Dears,\nSetting Configuration:\nWhat is your language?',
                                     reply_markup=reply_markup)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("1411672576:AAEUjpL4fFVZs7iNvmcxQBwLISqNc_mky3k", use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram

    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))

    dispatcher.add_handler(CallbackQueryHandler(button))
    result_chosen_handler = ChosenInlineResultHandler(on_result_chosen)
    dispatcher.add_handler(result_chosen_handler)
    inline_query_handler = InlineQueryHandler(get_inline_results)
    dispatcher.add_handler(inline_query_handler)

    updater.dispatcher.add_handler(MessageHandler(Filters.status_update.new_chat_members, new_member))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


from database import Database

if __name__ == '__main__':
    db = db = Database(
        'localhost',
        'splitDatabase',
        5432,
        'freez',
        'freez'
    )
    c = db.get_connection()
    main()
