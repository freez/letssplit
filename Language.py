from enum import Enum


class Language(Enum):
    Persian = 1
    English = 2
