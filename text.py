help_fa = "این بات برای مدیریت مالی با دوستاتون توسعه داده شده.\nکار کردن با این بات خیلی راحته. اول باید توی گروهتون اضافش کنید و هر خریدی که هر عضوی از گروه میکنه رو توسط این بات توی گروه ثبت کنید. این بات سهم هر نفر رو براتون حساب میکنه و گزارش‌هایی رو هم بهتون میده.\n\nاز دستورای زیر میتونید استفاده کنید:\n/setting\n برای تنظیماتی مثل زبان، واحد پول، و یا ساده سازی بدهی ها\n/users\nلیست کاربرانی که در گروه در این بات ثبت نام کردن\n/balances\nوضعیت بدهی و طلب هر یک از کاربران\n/bills\nلیست تمام خریدهای انجام شده\n/newbill\n ثبت خرید جدید\n/settleup\n وقتی با یک نفر تسویه حساب کردید از این دستور استفاده کنید\n"


def get_help(language) -> str:
    if language == "fa":
        return help_fa


def get_choose_money_unit(language):
    if language == "fa":
        return "واحد پول خریدهای خود را مشخص کنید:"
    else:
        return "Please determine your money unit."


def get_simplify(language):
    if language == "fa":
        return "آیا خلاصه سازی انجام شود؟\nمثال:\nعلی ۱۰۰۰۰ تومان به حسن بدهکار هست.\nحسن ۱۰۰۰۰ تومان به محمد بدهکار " \
               "است.\nدر صورت فعال سازی خلاصه سازی: علی ۱۰۰۰۰ تومان به محمد بدهکار است. "


def get_string(tag: str, language="fa") -> str:
    lower_tag = tag.lower()
    if lower_tag == "help":
        return get_help(language)
    elif lower_tag == "choose_money_unit":
        return get_choose_money_unit(language)
    elif lower_tag == "setting_simplify":
        return get_simplify(language)
