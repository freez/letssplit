import psycopg2


class Database:
    def __init__(self, host, db, port, user, pw):
        self.host = host
        self.db = db
        self.port = port
        self.user = user
        self.pw = pw

    def get_connection(self):
        return Connection(
            self.host,
            self.db,
            self.port,
            self.user,
            self.pw
        )


class Connection:
    def __init__(self, host, db, port, user, pw):
        conn = psycopg2.connect(
            "postgresql://{}:{}@{}:{}/{}".format(
                user, pw, host, port, db
            )
        )
        conn.autocommit = True
        self.cursor = conn.cursor()


class Transaction:
    def __init__(self, connection):
        self.cursor = connection.cursor

    def __enter__(self):
        self.cursor.execute("BEGIN;")
        self.is_error = False
        return self

    def __exit__(self, type, value, traceback):
        if self.is_error:
            self.cursor.execute("ROLLBACK;")
            return

        self.cursor.execute("COMMIT;")

    def add_new_group(self, group_id: int, name: str):
        pass
