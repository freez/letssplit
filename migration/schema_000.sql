CREATE TABLE users (
	id INTEGER PRIMARY KEY,
	telegram_id INTEGER,
	first_name TEXT,
	last_name TEXT,
	username TEXT,
	lang SMALLINT,
	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

CREATE TABLE groups (
	id INTEGER PRIMARY KEY,
	telegram_id INTEGER,
	name TEXT,
	money_unit TEXT,
	lang SMALLINT,
	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

CREATE TABLE usersInGroups (
	user_id INTEGER REFERENCES users(id),
	group_id INTEGER REFERENCES groups(id),
	PRIMARY KEY (user_id, group_id)
);

CREATE TABLE transactions (
	id SERIAL PRIMARY KEY,
	amount REAL NOT NULL,
	description VARCHAR(255),
	who_paid_id INTEGER REFERENCES users(id),
	group_id INTEGER REFERENCES groups(id),
	user_create_id INTEGER REFERENCES users(id),
	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	is_settle_up BOOLEAN DEFAULT FALSE,
	is_deleted BOOLEAN DEFAULT FALSE
);

CREATE TABLE userInTransactions (
	t_id INTEGER REFERENCES transactions(id),
	u_id INTEGER REFERENCES users(id),
	amountShare REAL NOT NULL,
	is_settle_up BOOLEAN DEFAULT FALSE,
	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	settled_up_at TIMESTAMP WITH TIME ZONE
);

